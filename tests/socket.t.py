import logging
logging.getLogger('socketIO-client').setLevel(logging.DEBUG)
logging.basicConfig()
from socketIO_client import SocketIO, LoggingNamespace
import json

with SocketIO('localhost', 8080, namespace='/sockets_sessions') as socketIO:
    socketIO.emit("add_session", {'fileUri': 'FILEURI', 'duration': 2000}, namespace='/sockets_sessions')
    socketIO.wait(seconds=1)
