import index


def test_test():
    index.app.testing = True
    client = index.app.test_client()

    r = client.get('/')
    assert r.status_code == 200
    assert 'Hello World' in r.data.decode('utf-8')
