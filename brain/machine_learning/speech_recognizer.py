from google.cloud import speech
from google.cloud.speech import enums
from google.cloud.speech import types
import logging
from flask import current_app
from brain.datastore import sessions_datastore as ds
import onesignal as onesignal_sdk

def init_app(app):
    app.audioTypeConfig = {
            'audio/vnd.wav' : enums.RecognitionConfig.AudioEncoding.LINEAR16,
            'audio/vnd.wave': enums.RecognitionConfig.AudioEncoding.LINEAR16,
            'audio/x-wav'   : enums.RecognitionConfig.AudioEncoding.LINEAR16,
            'audio/3gpp'    : enums.RecognitionConfig.AudioEncoding.AMR_WB,
            'audio/3gp'     : enums.RecognitionConfig.AudioEncoding.AMR_WB
        }

def _process_result(response):
    # Each result is for a consecutive portion of the audio. Iterate through
    # them to get the transcripts for the entire audio file.
    transcript = ''
    for result in response.results:
        # The first alternative is the most likely one for this portion.
        if len(result.alternatives) > 0:
            transcript += result.alternatives[0].transcript
    logging.info("Transcript = %s" % transcript);
    return transcript

# input - fileUri, audio type
# output - textual transcript
def shortAudioTranscription(fileUri, type):
    config = types.RecognitionConfig(
        encoding = current_app.audioTypeConfig[type],
        sample_rate_hertz=16000,
        language_code='en-US')
    #TODO: may be move client to app ?
    client = speech.SpeechClient()
    logging.info("File uri: %s" % fileUri)
    audio = types.RecognitionAudio(uri=fileUri)
    logging.info("Sending request to speech client")
    response = client.recognize(config, audio)
    logging.info("Received response from google api = %s" % response.results)
    return _process_result(response)

class LongRunningCallback:
    def __init__(self, user_id, profile_id, session_id, device_id, public_file_uri):
        self.user_id = user_id
        self.profile_id = profile_id
        self.session_id = session_id
        self.device_id = device_id
        self.one_signal_client = onesignal_sdk.Client(user_auth_key="OWYwMTQzZGEtMjg0Mi00ZTM5LTk5MjktMmE5ODkyM2FjNTcw",
                                            app={"app_auth_key": "YjlkN2VlOGUtYTQzMS00MDgwLWEwMGQtMGMxMmM4YzFkOTBk",
                                            "app_id": "279b7811-98eb-4a2f-aed4-573c745ccf9b"})
        self.public_file_uri = public_file_uri
    def __call__(self, operation_future):
        try:
            response = operation_future.result()
            transcript = _process_result(response)
        except Exception as e:
            logging.error("Error processing transcript: %s" % str(e))
            transcript = "Unable to transcribe session."
        # write to db
        logging.info("Long running transcript: %s", transcript)
        updated = ds.update_recording_session(self.user_id, self.profile_id, self.session_id, transcript, self.public_file_uri)
        if not updated:
            logging.info("No session to update. Will not send notification.")
            return
        if self.device_id is None:
            logging.info("No device id, not triggering notification")
            return
        logging.info("Device ID: %s", self.device_id)
        new_notification = onesignal_sdk.Notification(contents={"en": "Your session summary is ready for review"})
        new_notification.set_parameter("headings", {"en": "Session Summary Available"})
        new_notification.set_target_devices([self.device_id])
        new_notification.set_parameter("data", { "session_id": self.session_id })
        new_notification.set_parameter("buttons", [{"id": "view", "text": "View"}])
        response = self.one_signal_client.send_notification(new_notification)
        if response.status_code != 200:
            logging.error("Unable to send notification. Reason= %s", response.json())


def longAudioTranscription(fileUri, type, user_id, profile_id, session_id, device_id, public_file_uri=None):
    audio = types.RecognitionAudio(uri=fileUri)
    config = types.RecognitionConfig(
        encoding = current_app.audioTypeConfig[type],
        sample_rate_hertz=16000,
        language_code='en-US')

    client = speech.SpeechClient()
    operation = client.long_running_recognize(config, audio)
    logging.info("Adding callback to process async result")
    operation.add_done_callback(LongRunningCallback(user_id, profile_id, session_id, device_id, public_file_uri))
