from flask import current_app
import spacy
from spacy.matcher import PhraseMatcher
import logging
from spacy.tokens.doc import Doc
import time
import en_core_web_sm
from spacy.tokenizer import Tokenizer
import re

def custom_tokenizer(nlp):
    prefix_re = re.compile(r'\s')
    return Tokenizer(nlp.vocab, suffix_search = prefix_re.search)

def readMedicalTermsRaw(medicalTermsFile, nlp):
    with open(medicalTermsFile) as f:
        return [ nlp.make_doc(line.strip()) for line in f ]

def init_app(app):
    app.nlp = en_core_web_sm.load()
    logging.info("Custom tokenizer")
    app.nlp.tokenizer = custom_tokenizer(app.nlp)
    logging.info("PhraseMatcher")
    app.phraseMatcher = PhraseMatcher(app.nlp.vocab)

    logging.info("Reading medicalTerms")
    start = time.time()
    app.medicalTerms = readMedicalTermsRaw(app.config['MEDICAL_TERMS_FILE'], app.nlp)
    end = time.time()
    logging.info("Time to load docs: %f (s)" %(end - start))

    logging.info("Medical terms: %d" % len(app.medicalTerms))
    start = time.time()
    app.phraseMatcher.add('PHRASES', None, *app.medicalTerms)
    end = time.time()
    logging.info("Time to add docs: %f (s)" %(end - start))

# @input - transcript (raw text)
# @output - list of spans ( each span is indexed by words (start, end))
# assumes white space split
def findMedicalTerms(text):
    text = text.lower()
    doc = current_app.nlp.make_doc(text)
    matches = [ (match[1], match[2]) for match in current_app.phraseMatcher(doc) ]
    matches_sorted = sorted(matches, key=lambda x: x[1], reverse = True)
    logging.info("Matches Sorted: %s" % matches_sorted)
    matches_clean = removeOverlaps(matches_sorted)
    matches_clean_sorted = sorted(matches_clean, key=lambda x: x[0])
    logging.info("Matches clean and sorted: %s" % matches_clean_sorted)

    return matches_clean_sorted

def overlap(interval1, interval2):
    return interval2[0] >= interval1[0] and interval2[1] <= interval1[1]

# @input - list of tuples [(start, end)]
def removeOverlaps(matches):
    matches_clean = []
    if matches:
        matches_clean = [matches[0]]
        for x in matches[1:]:
            logging.info("Matches clean: %s" % matches_clean)
            if not overlap(matches_clean[-1], x):
                matches_clean.append(x)

    logging.info("Matches clean: %s" % matches_clean)
    return matches_clean
