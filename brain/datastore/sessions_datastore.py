from flask import current_app
from datetime import datetime
import uuid
import logging
from brain.datastore import get_client
from brain.datastore import datastore


def init_app(app):
    pass

def _from_datastore(entity):
    """Translates Datastore results into the format expected by the
    application.

    Datastore typically returns:
        [Entity{key: (kind, id), prop: val, ...}]

    This returns:
        {id: id, prop: val, ...}
    """
    if not entity:
        return None
    if isinstance(entity, list):
        entity = entity.pop()

    entity['id'] = entity.key.id_or_name
    return entity

def add_recording_session(user_id, profile_id, recording_file_uri, transcript, session_id=None):
    ds = get_client()
    if not session_id:
        session_id = str(uuid.uuid1())
    key = ds.key('User', user_id, 'Profile', profile_id, 'Session', session_id)
    session = {'file_uri': recording_file_uri, 'transcript': transcript, 'timestamp': datetime.utcnow()}
    entity = datastore.Entity(
        key=key,
        exclude_from_indexes=['transcript', 'file_uri'])
    entity.update(session)
    ds.put(entity)
    session.update({'id': session_id})
    return session

def get_list_of_sessions(user_id, profile_id, limit=10, cursor=None):
    ds = get_client()
    ancestor = ds.key('User', user_id, 'Profile', profile_id)
    query = ds.query(kind='Session', ancestor=ancestor, order=['-timestamp'])
    query_iterator = query.fetch(limit=limit, start_cursor=cursor)
    page = next(query_iterator.pages)

    entities = list(map(_from_datastore, page))
    next_cursor = (
        query_iterator.next_page_token.decode('utf-8')
        if query_iterator.next_page_token else None)

    return entities, next_cursor

def view_recording_session(user_id, profile_id, session_id):
    ds = get_client()
    key = ds.key('User', user_id, 'Profile', profile_id, 'Session', session_id)
    results = ds.get(key)
    return _from_datastore(results)

def update_recording_session(user_id, profile_id, session_id, transcript, public_file_uri=None):
    ds = get_client()
    key = ds.key('User', user_id, 'Profile', profile_id, 'Session', session_id)
    entity = ds.get(key)
    if entity:
        logging.info("Updating transcript for key: %s to %s" % (key, transcript))
        entity['transcript'] = transcript
        if public_file_uri:
            entity['file_uri'] = public_file_uri
        ds.put(entity)
        return True
    else:
        logging.warn("Update for %s failed. No such key found." % key)
        return False

def delete_recording_session(user_id, profile_id, session_id):
    ds = get_client()
    key = ds.key('User', user_id, 'Profile', profile_id, 'Session', session_id)
    ds.delete(key)
