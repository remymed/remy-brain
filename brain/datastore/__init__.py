from google.cloud import datastore
from flask import current_app
import os

# Now this client functionality
# can be used by all datastore files
# in this package
def get_client():
    return datastore.Client(os.environ['PROJECT_ID'])
