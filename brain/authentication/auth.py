from functools import wraps
from flask import g, request, redirect, url_for
from firebase_admin import auth
import logging
import os
from werkzeug.exceptions import HTTPException

def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        logging.info("Request: %s" % request)
        data = request.form.to_dict(flat=True)
        logging.info("Data: %s" % data)
        id_token = data.get('user_auth_token', None)
        if id_token:
            logging.info("id token: %s" % id_token)
            # TODO: raise appropriate exception type
            decoded_token = auth.verify_id_token(id_token)
            logging.info("decoded token: %s" % decoded_token)
            g.user_id = decoded_token['uid']
        elif os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine/'):
            #prod
            raise HTTPException("User authentication failed. No auth token provided.")
        else:
            # for testing locally
            g.user_id = 1
        g.profile_id = data.get('profile_id', 1)
        return f(*args, **kwargs)
    return decorated_function

def getUserId(id_token):
    decoded_token = auth.verify_id_token(id_token)
    logging.info("decoded token: %s" % decoded_token)
    return decoded_token['uid']
