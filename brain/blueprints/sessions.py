from flask import Blueprint, redirect, request, jsonify, g
from brain.authentication import auth
from brain.storage import sessions_storage as ss
from brain.storage import playback_storage as ps
from brain.datastore import sessions_datastore as ds
from brain.machine_learning import speech_recognizer as sr
from brain.machine_learning import text_analysis as ta
import logging
from flask import current_app
from datetime import datetime, timedelta
import uuid
sessions = Blueprint('sessions', __name__)

@sessions.route("/", methods=['GET', 'POST'])
@auth.login_required
def list():
    logging.info("Request: %s" % request)
    data = request.form.to_dict(flat=True)
    limit = int(data.get('limit', 10))
    cursor_token = data.get('cursor_token', None)
    sessions, next_page_token = ds.get_list_of_sessions(g.user_id, g.profile_id, limit, cursor_token)
    logging.info("Sessions returned: %s" % sessions)
    logging.info("Next page token: %s" % next_page_token)

    return jsonify({'sessions': sessions, 'cursor':next_page_token})

@sessions.route('/<id>', methods=['GET', 'POST'])
@auth.login_required
def view(id):
    logging.info("Session id to view: %s" % id)
    session = ds.view_recording_session(g.user_id, g.profile_id, id)
    if not session:
        return "Requested session not found."
    logging.info("Starting Phrase matching")
    matches = ta.findMedicalTerms(session['transcript'])
    session['phraseMatches'] = matches
    fileUri = session['file_uri']
    if fileUri[:4] == 'http': # check for remote uri
        logging.info("Starting to get signed url for playback")
        expiration = (datetime.now() + timedelta(hours=48)).strftime('%s') # epoch time 48 hours from now
        signedUrl = ps.getSignedURLForPlayback(fileUri, expiration)
        logging.info("Signed URl for playback: %s" % signedUrl)
        session['file_uri'] = signedUrl
    logging.info("Session id %s session is %s" %(id, session))
    return jsonify(session)

@sessions.route('/add', methods=['POST'])
@auth.login_required
def addRecording():
    # Expected: {'user_auth_token': <token>, 'profile_id': <id>, 'file': <recording_file>}
    logging.info("Request: %s" % request.files)
    file = request.files['file']
    # filename coming from UI is a uuid - we will split on '.' take the file extension and
    # generate a uuid for the file name
    url_safe_filename = str(uuid.uuid1()) + '.' + file.filename.split('.')[-1]
    logging.info("Filename %s, URL safe Filename %s" % (file.filename, url_safe_filename))
    logging.info("Content type: %s" % file.content_type)
    public_file_uri, gs_uri = ss.upload_file(file.stream.read(), url_safe_filename, file.content_type)
    logging.info("Urls from storage: %s, %s" % (public_file_uri, gs_uri))

    logging.info("Starting transcription")
    data = request.form.to_dict(flat=True)
    defaultDur = current_app.config['MAX_SYNC_RECORDING_DURATION'] + 1
    logging.info("Default dur: %d" % defaultDur)
    recording_duration = int(data.get('duration', defaultDur))
    device_id = data.get('device_id', None)
    if recording_duration <= current_app.config['MAX_SYNC_RECORDING_DURATION']:
        transcript = sr.shortAudioTranscription(gs_uri, file.content_type)
    else:
        transcript = "Transcript with be available shortly."
    session = ds.add_recording_session(g.user_id, g.profile_id, public_file_uri, transcript)
    logging.info("Starting Phrase matching")
    matches = ta.findMedicalTerms(transcript)
    session['phraseMatches'] = matches
    logging.info("Starting to get signed url for playback")
    expiration = (datetime.now() + timedelta(hours=48)).strftime('%s') # epoch time 48 hours from now
    signedUrl = ps.getSignedURLForPlayback(public_file_uri, expiration)
    logging.info("Signed URl for playback: %s" % signedUrl)
    session['file_uri'] = signedUrl
    logging.info("Session returned: %s" % session)
    if recording_duration > current_app.config['MAX_SYNC_RECORDING_DURATION']:
        # kick off async
        logging.info("Kicking off long running async")
        sr.longAudioTranscription(gs_uri, file.content_type, g.user_id, g.profile_id, session['id'],
          device_id)
    return jsonify(session)


@sessions.route('/delete/<id>', methods=['POST'])
@auth.login_required
def delete(id):
    session = ds.delete_recording_session(g.user_id, g.profile_id, id)
    logging.info("Session id %s deleted" % id)
    resp = { 'deleted' :  id }
    return jsonify(resp)

@sessions.route('/addSimple', methods=['POST'])
@auth.login_required
def addSimple():
    data = request.form.to_dict(flat=True)
    logging.info("Request data: %s" % data)
    session = ds.add_recording_session(g.user_id, g.profile_id, data['fileUri'], data['transcript'],  data['id'])
    return jsonify(session)

@sessions.route('/startTranscription', methods=['POST'])
@auth.login_required
def startTranscription():
    data = request.form.to_dict(flat=True)
    logging.info("Request data: %s" % data)
    gs_uri = data['fileUri']
    public_file_uri = gs_uri.replace('gs://', 'https://storage.googleapis.com/')
    logging.info("Public uri: %s, gs_uri: %s" % (public_file_uri, gs_uri))
    logging.info("Kicking off long running async")
    sr.longAudioTranscription(gs_uri, 'audio/x-wav', g.user_id, g.profile_id, data['id'], data.get('device_id', None), public_file_uri)
    return jsonify({'id': data['id']})
