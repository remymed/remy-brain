from __future__ import absolute_import

import datetime

from flask import current_app
import six
from werkzeug import secure_filename
from werkzeug.exceptions import BadRequest
import logging
from brain.storage import get_storage_client, get_storage_bucket

def _check_extension(filename, allowed_extensions):
    if ('.' not in filename or
            filename.split('.').pop().lower() not in allowed_extensions):
        raise BadRequest(
            "{0} has an invalid name or extension".format(filename))


def _safe_filename(filename):
    """
    Generates a safe filename that is unlikely to collide with existing objects
    in Google Cloud Storage.

    ``filename.ext`` is transformed into ``filename-YYYY-MM-DD-HHMMSS.ext``
    """
    filename = secure_filename(filename)
    date = datetime.datetime.utcnow().strftime("%Y-%m-%d-%H%M%S")
    basename, extension = filename.rsplit('.', 1)
    return "{0}-{1}.{2}".format(basename, date, extension)

def _get_gs_url(blob):
    return 'gs://' + blob.bucket.name + '/' + blob.name

# [START upload_file]
def upload_file(file_stream, filename, content_type):
    """
    Uploads a file to a given Cloud Storage bucket and returns the public url
    to the new object.
    """
    _check_extension(filename, current_app.config['ALLOWED_EXTENSIONS'])
    filename = _safe_filename(filename)

    client = get_storage_client()
    bucket = get_storage_bucket(client)
    blob = bucket.blob(filename)

    blob.upload_from_string(
        file_stream,
        content_type=content_type)

    logging.info("Storage blob: %s" % dir(blob))
    logging.info("Storage url: %s" % blob.path)
    url = blob.public_url

    if isinstance(url, six.binary_type):
        url = url.decode('utf-8')

    gs_url = _get_gs_url(blob)
    return url, gs_url
# [END upload_file]
