from __future__ import absolute_import

import datetime

from flask import current_app
from werkzeug.exceptions import BadRequest
from brain.storage import get_storage_client, get_storage_bucket
from oauth2client.service_account import ServiceAccountCredentials
import os, urllib, base64, logging

def _get_base_object_path(public_file_url):
    return urllib.parse.urlparse(public_file_url).path

def _create_signature_string(http_verb, expiration, baseObjectPath):
    return '\n'.join([http_verb, '', '', expiration, baseObjectPath])

def _sign_signature_string(signatureString):
    creds = ServiceAccountCredentials.from_json_keyfile_name('Remy-b9e28dcc36e1.json')
    logging.info("creds service_account: %s" % creds.service_account_email)
    logging.info("env check: %s" % os.getenv('GOOGLE_APPLICATION_CREDENTIALS', "NOT_SET"))
    client_id = creds.service_account_email
    signature = creds.sign_blob(signatureString)[1]
    logging.info("signature: %s" % signature)
    return base64.b64encode(signature), client_id

# @input - @public_file_url - public url returned after file is successfully uploaded
# @input - @expiration - number of seconds after which the signed url access expires
# @output - signed url to be sent to the client
def getSignedURLForPlayback(public_file_url, expiration):
    # get base bucket and object info
    baseObjectPath = _get_base_object_path(public_file_url)
    # create signature string
    signatureString = _create_signature_string('GET', expiration, baseObjectPath)
    logging.info("Signature string: %s" % signatureString)
    # sign the signature string
    signedString, client_id = _sign_signature_string(signatureString)
    # combine base public url and signed string
    signedUrl = public_file_url + "?GoogleAccessId=" + client_id + "&Expires=" + expiration + "&Signature=" + urllib.parse.quote_plus(signedString.decode('utf-8'))

    return signedUrl
