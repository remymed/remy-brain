from google.cloud import storage
from flask import current_app

# Now this client functionality
# can be used by all storage files
# in this package
def get_storage_client():
    return storage.Client(project=current_app.config['PROJECT_ID'])

def get_storage_bucket(client):
    return client.bucket(current_app.config['CLOUD_STORAGE_BUCKET'])
