import logging

from flask import current_app, Flask, redirect, url_for, jsonify
from brain.machine_learning import speech_recognizer
from brain.blueprints import sessions
from brain.datastore import sessions_datastore
from werkzeug.exceptions import HTTPException
from brain.machine_learning import text_analysis
import firebase_admin

def create_app(config, debug=False, config_overrides=None):
    app = Flask(__name__)
    app.config.from_object(config)
    if config_overrides:
        app.config.update(config_overrides)

    app.firsebase = firebase_admin.initialize_app()
    app.debug = debug

    # Configure logging
    if app.debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)


    logging.info("Intialize datastore")
    sessions_datastore.init_app(app)

    logging.info("Intialize speech_recognizer")
    speech_recognizer.init_app(app)

    logging.info("Intialize text_analysis")
    text_analysis.init_app(app)

    app.register_blueprint(sessions.sessions, url_prefix='/sessions')

    return app
