import brain
import config
import time
import logging

start = time.time()
app = brain.create_app(config)
end = time.time()
logging.info("App startup time: %f (s)" %(end - start))

# This is only used when running locally. When running live, gunicorn runs
# the application.
if __name__ == '__main__':
    app.run(host='127.0.0.1', port=8080, debug=True)
