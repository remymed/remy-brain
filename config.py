"""
This file contains all of the configuration values for the application.
"""

import os

# The secret key is used by Flask to encrypt session cookies.
SECRET_KEY = 'secret'

# Google Cloud Project ID. This can be found on the 'Overview' page at
# https://console.developers.google.com
PROJECT_ID = 'remy-1526162221045'

CLOUD_STORAGE_BUCKET = 'remy-1526162221045.appspot.com'

ALLOWED_EXTENSIONS = set(['wav', 'wave', '3gp', '3gp2'])

MEDICAL_TERMS_FILE = 'medical-terms/filtered_cleaned_categories_terms.txt'

MAX_SYNC_RECORDING_DURATION = 10000 # in milli seconds
