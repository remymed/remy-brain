# remy-brain


## Running local server

1. Google credentials need to be setup. Navigate to the google console.
`https://console.cloud.google.com/apis/credentials?project=speech-analysis-project`
2. Create an API key and download it.
3. Set your environment variable before running
```
export GOOGLE_APPLICATION_CREDENTIALS=~/Downloads/Remy-b9e28dcc36e1.json`

export PROJECT_ID="remy-1526162221045"
```
4. (optional for first time) Create new blank environment
`conda create -n remy-brain python=3 `
5. Activate your environment
`source activate remy-brain`
5. Run
` pip install -r requirements.txt `
6. To start the server, run -
`python main.py`


## CURL command for testing remy-brain

To CURL to send test file
```
curl -H "Content-Type: multipart/form-data" -X POST -F "file=@recording_file.wav;type=audio/vnd.wav" http://localhost:8080/sessions/add

```

## Deployment

Run the command below in the top level directory with the `app.yaml` file

```
gcloud app deploy
```

## Data Sources

Clinical terms v/s medical terms

1. Medical literature - http://bio.nlplab.org/ - PubMed and PMC
   * WordVectors
   * N-gram counts
   * Language Model - Using KenLM

2. i2b2 NLP challenge - clinical records - https://www.i2b2.org/NLP/DataSets/AgreementCR.php
   * Patient release notes
   * ~1500

3. https://mimic.physionet.org/gettingstarted/access/
   * Need to finish a course before requesting access
